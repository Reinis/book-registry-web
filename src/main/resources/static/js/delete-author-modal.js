$("#delete_author_modal").on("show.bs.modal", (event) => {
    // Find the row with the pressed button
    let row = $(event.relatedTarget).closest("tr");

    // Get cell data
    let authorId = row.find("td:nth-child(1)").text();
    let authorFirstName = row.find("td:nth-child(2)").text();
    let authorLastName = row.find("td:nth-child(3)").text();

    // Update author details in the modal window
    let modal = $("#delete_author_modal");
    modal.find(".modal-body #modal__author-id").text(authorId);
    modal.find(".modal-body #modal__author-first-name").text(authorFirstName);
    modal.find(".modal-body #modal__author-last-name").html(authorLastName);

    let deleteButton = modal.find(".modal-footer #modal__delete-button");
    let authorBase = deleteButton.attr("href");
    deleteButton.attr(
        "href",
        authorBase.replace(/list$/, "delete?authorId=" + authorId)
    );
});
