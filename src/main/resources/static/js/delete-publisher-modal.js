$("#delete_publisher_modal").on("show.bs.modal", (event) => {
    // Find the row with the pressed button
    let row = $(event.relatedTarget).closest("tr");

    // Get cell data
    let publisherId = row.find("td:nth-child(1)").text();
    let publisherName = row.find("td:nth-child(2)").text();

    // Update details in the modal window
    let modal = $("#delete_publisher_modal");
    modal.find(".modal-body #modal__publisher-id").text(publisherId);
    modal.find(".modal-body #modal__publisher-name").text(publisherName);

    let deleteButton = modal.find(".modal-footer #modal__delete-button");
    let publisherBase = deleteButton.attr("href");
    deleteButton.attr(
        "href",
        publisherBase.replace(/list$/, "delete?publisherId=" + publisherId)
    );
});
