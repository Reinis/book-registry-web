$("#delete_book_modal").on("show.bs.modal", (event) => {
    // Find the row with the pressed button
    let row = $(event.relatedTarget).closest("tr");

    // Get cell data
    let bookId = row.find("td:nth-child(1)").text();
    let bookTitle = row.find("td:nth-child(2)").text();
    let bookAuthors = row.find("td:nth-child(3)").html();
    let bookYearPublished = row.find("td:nth-child(4)").text();
    let bookPublisher = row.find("td:nth-child(5)").text();

    // Update book details in the modal window
    let modal = $("#delete_book_modal");
    modal.find(".modal-body #modal__book-id").text(bookId);
    modal.find(".modal-body #modal__book-title").text(bookTitle);
    modal.find(".modal-body #modal__book-authors").html(bookAuthors);
    modal
        .find(".modal-body #modal__book-year-published")
        .text(bookYearPublished);
    modal.find(".modal-body #modal__book-publisher").text(bookPublisher);

    let deleteButton = modal.find(".modal-footer #modal__delete-button");
    let bookBase = deleteButton.attr("href");
    deleteButton.attr(
        "href",
        bookBase.replace(/list$/, "delete?bookId=" + bookId)
    );
});
