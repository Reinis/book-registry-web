package com.github.reinis.bookregistryweb.entity;

import javax.persistence.*;

@Entity
@Table(name = "publishers")
public class Publisher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "publisher_id")
    private int id;

    @Column(name = "name")
    private String name = "";

    // Required by Hibernate
    public Publisher() {
    }

    public Publisher(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /* The equals function has to have these properties:
     *   1. Reflexive - equal to self.
     *   2. Symmetric - equal both ways for two entities.
     *   3. Transitive - all three equal if two are equal.
     *   4. Consistent - return the same result for all entity states.
     *   5. Return false for new (transient) entities (id=0).
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publisher publisher = (Publisher) o;
        return id != 0 && id == publisher.getId();
    }

    /* The hashCode has to be consistent across entity state transitions.
     *
     * Note: Constant hash code could be a problem for large maps, but
     *       Hibernate is not designed to work with large maps.
     */
    @Override
    public int hashCode() {
        return 42;
    }

    @Override
    public String toString() {
        return "Publisher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
