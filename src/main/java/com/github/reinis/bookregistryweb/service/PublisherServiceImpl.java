package com.github.reinis.bookregistryweb.service;

import com.github.reinis.bookregistryweb.dao.PublisherRepository;
import com.github.reinis.bookregistryweb.entity.Publisher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PublisherServiceImpl implements PublisherService {

    private final PublisherRepository publisherRepository;

    @Override
    public List<Publisher> findAll() {
        return publisherRepository.findAll();
    }

    @Override
    public Publisher findById(int theId) {
        Optional<Publisher> publisher = publisherRepository.findById(theId);

        if (publisher.isPresent()) {
            return publisher.get();
        } else {
            throw new RuntimeException("Could not find publisher id - " + theId);
        }
    }

    @Override
    public void save(Publisher thePublisher) {
        publisherRepository.save(thePublisher);
    }

    @Override
    public void deleteById(int theId) {
        publisherRepository.deleteById(theId);
    }

    @Override
    public List<Publisher> findAllOrderByNameAsc() {
        return publisherRepository.findAllByOrderByNameAsc();
    }

    @Override
    public List<Publisher> findByNameContaining(String searchTerm) {
        return publisherRepository.findByNameContainingIgnoreCase(searchTerm);
    }

    @Override
    public Publisher findByName(String name) {
        return publisherRepository.findByName(name);
    }
}
