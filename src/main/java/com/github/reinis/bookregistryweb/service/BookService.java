package com.github.reinis.bookregistryweb.service;

import com.github.reinis.bookregistryweb.entity.Book;

import java.util.List;

public interface BookService {
    List<Book> findAll();

    Book findById(int theId);

    void save(Book theBook);

    void deleteById(int theId);

    List<Book> searchByTitleContaining(String searchTerm);

    List<Book> findByPublisherContaining(String searchTerm);

    List<Book> findByYear(Integer searchTerm);

    List<Book> searchByAuthorContaining(String searchTerm);

    Book findByTitle(String title);
}
