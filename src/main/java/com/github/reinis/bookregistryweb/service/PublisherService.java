package com.github.reinis.bookregistryweb.service;

import com.github.reinis.bookregistryweb.entity.Publisher;

import java.util.List;

public interface PublisherService {
    List<Publisher> findAll();

    Publisher findById(int theId);

    void save(Publisher thePublisher);

    void deleteById(int theId);

    List<Publisher> findAllOrderByNameAsc();

    List<Publisher> findByNameContaining(String searchTerm);

    Publisher findByName(String name);
}
