package com.github.reinis.bookregistryweb.service;

import com.github.reinis.bookregistryweb.dao.AuthorRepository;
import com.github.reinis.bookregistryweb.entity.Author;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    @Override
    public List<Author> findAll() {
        return authorRepository.findAll();
    }

    @Override
    public Author findById(int theId) {
        Optional<Author> result = authorRepository.findById(theId);

        if (result.isPresent()) {
            return result.get();
        } else {
            throw new RuntimeException("Could not find author id - " + theId);
        }
    }

    @Override
    public void save(Author theAuthor) {
        authorRepository.save(theAuthor);
    }

    @Override
    public void deleteById(int theId) {
        authorRepository.deleteById(theId);
    }

    @Override
    public List<Author> findAllByOrderByLastNameAsc() {
        return authorRepository.findAllByOrderByLastNameAsc();
    }

    @Override
    public Author findByName(String name) {
        return authorRepository.findByName(name);
    }
}
