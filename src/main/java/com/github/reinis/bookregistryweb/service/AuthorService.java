package com.github.reinis.bookregistryweb.service;

import com.github.reinis.bookregistryweb.entity.Author;

import java.util.List;

public interface AuthorService {
    List<Author> findAll();

    Author findById(int theId);

    void save(Author theAuthor);

    void deleteById(int theId);

    List<Author> findAllByOrderByLastNameAsc();

    Author findByName(String name);
}
