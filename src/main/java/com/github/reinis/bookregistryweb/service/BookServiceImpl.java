package com.github.reinis.bookregistryweb.service;

import com.github.reinis.bookregistryweb.dao.BookRepository;
import com.github.reinis.bookregistryweb.entity.Book;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public Book findById(int theId) {
        Optional<Book> result = bookRepository.findById(theId);

        if (result.isPresent()) {
            return result.get();
        } else {
            throw new RuntimeException("Could not find book id - " + theId);
        }
    }

    @Override
    public void save(Book theBook) {
        bookRepository.save(theBook);
    }

    @Override
    public void deleteById(int theId) {
        bookRepository.deleteById(theId);
    }

    @Override
    public List<Book> searchByTitleContaining(String searchTerm) {
        return bookRepository.findByTitleContainingIgnoreCase(searchTerm);
    }

    @Override
    public List<Book> findByPublisherContaining(String searchTerm) {
        return bookRepository.findByPublisherNameContainingIgnoreCase(searchTerm);
    }

    @Override
    public List<Book> findByYear(Integer searchTerm) {
        return bookRepository.findByYearPublished(searchTerm);
    }

    @Override
    public List<Book> searchByAuthorContaining(String searchTerm) {
        Sort sort = Sort.by("id").ascending();

        return bookRepository.searchByAuthor(searchTerm, sort);
    }

    @Override
    public Book findByTitle(String title) {
        return bookRepository.findByTitle(title);
    }
}
