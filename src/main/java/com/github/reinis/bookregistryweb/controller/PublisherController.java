package com.github.reinis.bookregistryweb.controller;

import com.github.reinis.bookregistryweb.entity.Publisher;
import com.github.reinis.bookregistryweb.service.PublisherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/publishers")
@RequiredArgsConstructor
public class PublisherController {

    private final PublisherService publisherService;

    @RequestMapping
    public String mainPublishers(Model theModel) {
        return "redirect:/publishers/list";
    }

    @RequestMapping("/list")
    public String listPublishers(Model theModel) {
        List<Publisher> thePublishers = publisherService.findAll();

        theModel.addAttribute("publishers", thePublishers);

        return "publishers/publishers";
    }

    @RequestMapping("/editPublisher")
    public String showPublisherForm(@RequestParam Optional<Integer> bookId,
                                    @RequestParam Optional<Integer> publisherId,
                                    Model theModel) {
        Publisher thePublisher;

        if (publisherId.isPresent()) {
            thePublisher = publisherService.findById(publisherId.get());
        } else {
            thePublisher = new Publisher();
        }

        if (bookId.isPresent()) {
            theModel.addAttribute("bookId", bookId.get());
        }
        theModel.addAttribute("publisher", thePublisher);

        return "publishers/publisher-form";
    }

    @RequestMapping("/save")
    public String savePublisher(@ModelAttribute Publisher publisher,
                                @RequestParam Optional<Integer> bookId) {
        publisherService.save(publisher);

        if (bookId.isPresent()) {
            // We added a new publisher from the book form, so return there
            return "redirect:/books/showFormForUpdate?bookId=" + bookId.get();
        } else {
            return "redirect:/publishers/list";
        }
    }

    @RequestMapping("/delete")
    public String deletePublisher(@RequestParam int publisherId) {
        publisherService.deleteById(publisherId);

        return "redirect:/publishers/list";
    }
}
