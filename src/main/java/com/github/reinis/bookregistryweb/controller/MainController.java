package com.github.reinis.bookregistryweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping
    public String welcomePage(Model theModel) {
        return "redirect:/books/list";
    }
}
