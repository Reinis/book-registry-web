package com.github.reinis.bookregistryweb.controller;

import com.github.reinis.bookregistryweb.entity.Author;
import com.github.reinis.bookregistryweb.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping({"", "/"})
    public String mainAuthors(Model theModel) {
        return "redirect:/authors/list";
    }

    @GetMapping("/list")
    public String listAuthors(Model theModel) {
        // Get authors form the DB
        List<Author> theAuthors = authorService.findAll();

        theModel.addAttribute("authors", theAuthors);

        return "authors/authors";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model theModel) {
        // Create model attribute to bind form data
        Author theAuthor = new Author();

        theModel.addAttribute("author", theAuthor);

        return "authors/author-form";
    }

    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("authorId") int theId, Model theModel) {
        Author theAuthor = authorService.findById(theId);

        theModel.addAttribute("author", theAuthor);

        return "authors/author-form";
    }

    @PostMapping("/save")
    public String saveAuthor(@ModelAttribute("author") Author theAuthor) {
        authorService.save(theAuthor);

        // Use redirect to prevent duplicate submissions
        return "redirect:/authors/list";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("authorId") int theId) {
        authorService.deleteById(theId);

        return "redirect:/authors/list";
    }
}
