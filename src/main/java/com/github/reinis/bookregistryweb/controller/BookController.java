package com.github.reinis.bookregistryweb.controller;

import com.github.reinis.bookregistryweb.entity.Author;
import com.github.reinis.bookregistryweb.entity.Book;
import com.github.reinis.bookregistryweb.entity.Publisher;
import com.github.reinis.bookregistryweb.service.AuthorService;
import com.github.reinis.bookregistryweb.service.BookService;
import com.github.reinis.bookregistryweb.service.PublisherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final AuthorService authorService;

    private final BookService bookService;

    private final PublisherService publisherService;

    @GetMapping({"", "/"})
    public String mainBooks(Model theModel) {
        return "redirect:/books/list";
    }

    @GetMapping("/list")
    public String listBooks(Model theModel) {
        // Get form the DB
        List<Book> theBooks = bookService.findAll();

        theModel.addAttribute("books", theBooks);

        return "books/books";
    }

    @GetMapping("/search")
    public String searchBooks(
            @RequestParam("searchTerm") String theSearchTerm,
            @RequestParam("searchField") String theSearchField,
            Model theModel) {

        if (theSearchTerm == null ||
                theSearchTerm.isEmpty() || theSearchTerm.isBlank()) {
            return "redirect:/books/list";
        }

        if (theSearchField == null) {
            return "redirect:/books/list";
        }

        List<Book> theBooks;

        switch (theSearchField) {
            case "title":
                theBooks = bookService.searchByTitleContaining(theSearchTerm);
                break;
            case "author":
                theBooks = bookService.searchByAuthorContaining(theSearchTerm);
                break;
            case "year":
                int year;
                try {
                    year = Integer.parseInt(theSearchTerm);
                } catch (NumberFormatException e) {
                    return "redirect:/books/list";
                }
                theBooks = bookService.findByYear(year);
                break;
            case "publisher":
                theBooks = bookService.findByPublisherContaining(theSearchTerm);
                break;
            default:
                return "redirect:/books/list";
        }

        theModel.addAttribute("books", theBooks);
        theModel.addAttribute("searchField", theSearchField);

        return "books/books";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model theModel) {
        // Create model attribute to bind form data
        Book theBook = new Book();
        List<Author> allAuthors = authorService.findAllByOrderByLastNameAsc();
        List<Publisher> allPublishers = publisherService.findAllOrderByNameAsc();

        theModel.addAttribute("book", theBook);
        theModel.addAttribute("all_authors", allAuthors);
        theModel.addAttribute("all_publishers", allPublishers);

        return "books/book-form";
    }

    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("bookId") int theId, Model theModel) {
        Book theBook = bookService.findById(theId);
        List<Author> allAuthors = authorService.findAllByOrderByLastNameAsc();
        List<Publisher> allPublishers = publisherService.findAllOrderByNameAsc();

        theModel.addAttribute("book", theBook);
        theModel.addAttribute("all_authors", allAuthors);
        theModel.addAttribute("all_publishers", allPublishers);

        return "books/book-form";
    }

    @PostMapping("/save")
    public String saveBook(@ModelAttribute("book") Book theBook) {
        bookService.save(theBook);

        // Use redirect to prevent duplicate submissions
        return "redirect:/books/list";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("bookId") int theId) {
        bookService.deleteById(theId);

        return "redirect:/books/list";
    }
}
