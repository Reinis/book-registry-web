package com.github.reinis.bookregistryweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookRegistryWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookRegistryWebApplication.class, args);
	}

}
