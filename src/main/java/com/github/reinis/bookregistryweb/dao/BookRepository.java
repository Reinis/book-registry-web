package com.github.reinis.bookregistryweb.dao;

import com.github.reinis.bookregistryweb.entity.Book;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Integer> {
    @Query("from Book b where lower(b.title) like lower(concat('%', :searchTerm, '%'))")
    List<Book> searchBooksByTitle(@Param("searchTerm") String searchTerm);

    List<Book> findByTitleContainingIgnoreCase(String searchTerm);

    List<Book> findByPublisherNameContainingIgnoreCase(String searchTerm);

    List<Book> findByYearPublished(Integer searchTerm);

    // Fetch publishers and authors to reduce the number of database calls
    @Query("select distinct book from Book book " +
            "join fetch book.publisher " +
            "join fetch book.authors author " +
            "where " +
                "lower(author.firstName) like lower(concat('%', :searchTerm, '%')) " +
                "or lower(author.lastName) like lower(concat('%', :searchTerm, '%')) " +
            ""
    )
    List<Book> searchByAuthor(@Param("searchTerm") String searchTerm, Sort sort);

    @Query("select distinct book from Book book " +
            "where " +
                "lower(book.title) = lower(title) " +
            ""
    )
    Book findByTitle(@Param("title") String title);
}
