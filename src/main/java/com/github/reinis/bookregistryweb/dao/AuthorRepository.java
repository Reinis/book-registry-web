package com.github.reinis.bookregistryweb.dao;

import com.github.reinis.bookregistryweb.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AuthorRepository extends JpaRepository<Author, Integer> {
    // Spring Data JPA query method auto-generated from the method name
    // from Author order by lastName asc
    List<Author> findAllByOrderByLastNameAsc();

    // Find an author by full name
    // TODO: Account for an empty part of the name
    @Query("select distinct author from Author author " +
            "where " +
            "    lower(concat(author.firstName, ' ', author.lastName)) " +
            "    like lower(:name) " +
            ""
    )
    Author findByName(@Param("name") String name);
}
