package com.github.reinis.bookregistryweb.dao;

import com.github.reinis.bookregistryweb.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PublisherRepository extends JpaRepository<Publisher, Integer> {
    List<Publisher> findAllByOrderByNameAsc();

    List<Publisher> findByNameContainingIgnoreCase(String searchTerm);

    @Query("select publisher from Publisher publisher " +
            "where " +
            "    lower(publisher.name) = lower(:name) " +
            ""
    )
    Publisher findByName(@Param("name") String name);
}
