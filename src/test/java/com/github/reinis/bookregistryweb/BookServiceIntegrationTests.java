package com.github.reinis.bookregistryweb;

import com.github.reinis.bookregistryweb.dao.BookRepository;
import com.github.reinis.bookregistryweb.entity.Author;
import com.github.reinis.bookregistryweb.entity.Book;
import com.github.reinis.bookregistryweb.entity.Publisher;
import com.github.reinis.bookregistryweb.service.BookServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookServiceIntegrationTests {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookServiceImpl bookService;

    @BeforeEach
    void setUp() {
        Book testBook = new Book("Test book", 2020, new Publisher("My Books"));
        Author author = new Author("John", "Doe");
        testBook.setAuthors(List.of(author));

        when(bookRepository.findByTitle(testBook.getTitle()))
                .thenReturn(testBook);
    }

    @Test
    public void whenValidTitle_thenBookShouldBeFound() {
        String title = "Test book";
        Book found = bookService.findByTitle(title);

        assertThat(found.getTitle())
                .isEqualTo(title);
    }
}
