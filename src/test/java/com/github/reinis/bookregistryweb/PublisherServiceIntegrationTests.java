package com.github.reinis.bookregistryweb;

import com.github.reinis.bookregistryweb.dao.PublisherRepository;
import com.github.reinis.bookregistryweb.entity.Publisher;
import com.github.reinis.bookregistryweb.service.PublisherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PublisherServiceIntegrationTests {

    @Mock
    PublisherRepository publisherRepository;

    @InjectMocks
    PublisherServiceImpl publisherService;

    @BeforeEach
    void setUp() {
        Publisher tor = new Publisher("Tor Books");

        when(publisherRepository.findByName(tor.getName()))
                .thenReturn(tor);
    }

    @Test
    public void whenValidName_thenPublisherShouldBeFound() {
        String name = "Tor Books";
        Publisher found = publisherService.findByName(name);

        assertThat(found.getName())
                .isEqualTo(name);
    }
}
