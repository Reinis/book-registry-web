package com.github.reinis.bookregistryweb;

import com.github.reinis.bookregistryweb.dao.AuthorRepository;
import com.github.reinis.bookregistryweb.entity.Author;
import com.github.reinis.bookregistryweb.service.AuthorServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthorServiceIntegrationTests {

    @Mock
    AuthorRepository authorRepository;

    @InjectMocks
    private AuthorServiceImpl authorService;

    @BeforeEach
    void setUp() {
        Author alex = new Author("Alex", "Verus");

        when(authorRepository.findByName(alex.getName()))
                .thenReturn(alex);
    }

    @Test
    public void whenValidName_thenAuthorShouldBeFound() {
        String name = "Alex Verus";
        Author found = authorService.findByName(name);

        assertThat(found.getName())
                .isEqualTo(name);
    }
}
