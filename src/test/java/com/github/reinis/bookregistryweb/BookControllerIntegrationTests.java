package com.github.reinis.bookregistryweb;

import com.github.reinis.bookregistryweb.controller.BookController;
import com.github.reinis.bookregistryweb.entity.Author;
import com.github.reinis.bookregistryweb.entity.Book;
import com.github.reinis.bookregistryweb.entity.Publisher;
import com.github.reinis.bookregistryweb.service.AuthorService;
import com.github.reinis.bookregistryweb.service.BookService;
import com.github.reinis.bookregistryweb.service.PublisherService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(BookController.class)
public class BookControllerIntegrationTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AuthorService authorService;

    @MockBean
    private PublisherService publisherService;

    @MockBean
    private BookService bookService;

    // Expected content type
    private final MediaType mediaType = new MediaType(
            MediaType.TEXT_HTML.getType(),
            MediaType.TEXT_HTML.getSubtype(),
            StandardCharsets.UTF_8
    );


    @Test
    public void whenBooks_thenRedirectToBookList() throws Exception {
        mvc.perform(get("/books"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/books/list"))
                .andExpect(redirectedUrl("/books/list"));

        // Test also the URL with a trailing slash
        mvc.perform(get("/books/"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/books/list"))
                .andExpect(redirectedUrl("/books/list"));
    }

    @Test
    public void givenBooks_whenListBooks_thenReturnBookList() throws Exception {
        // Test data
        Book testBook = new Book("Test book", 2020, new Publisher("My Books"));
        Author author = new Author("John", "Doe");
        testBook.setAuthors(List.of(author));

        List<Book> allBooks = List.of(testBook);

        // Mock book service
        given(bookService.findAll())
                .willReturn(allBooks);

        // Perform request and test the response
        mvc.perform(get("/books/list"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaType))
                .andExpect(view().name("books/books"))
                .andExpect(model().attributeExists("books"))
                .andExpect(model().attribute("books", equalTo(allBooks)));
    }

    @Test
    public void whenAddBook_thenShowFormForAdd() throws Exception {
        // Test data
        Book newBook = new Book();

        Author author1 = new Author("Cadsuane", "Melaidhrin");
        Author author2 = new Author("Moiraine", "Damodred");
        Author author3 = new Author("Egwene", "al'Vere");
        List<Author> allAuthors = List.of(author1, author2, author3);

        Publisher publisher1 = new Publisher("Amyrlin Seat");
        Publisher publisher2 = new Publisher("Rhuidean");
        List<Publisher> allPublishers = List.of(publisher1, publisher2);

        // Mock author service
        given(authorService.findAllByOrderByLastNameAsc())
                .willReturn(allAuthors);

        // Mock publisher service
        given(publisherService.findAllOrderByNameAsc())
                .willReturn(allPublishers);

        mvc.perform(get("/books/showFormForAdd"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaType))
                .andExpect(view().name("books/book-form"))
                .andExpect(model().attributeExists("book", "all_authors", "all_publishers"))
                .andExpect(model().attribute("book",
                        hasProperty("id", is(0))))
                .andExpect(model().attribute("book",
                        hasProperty("title", is(nullValue()))))
                .andExpect(model().attribute("book",
                        hasProperty("yearPublished", is(0))))
                .andExpect(model().attribute("book",
                        hasProperty("publisher", is(nullValue()))))
                .andExpect(model().attribute("all_authors", equalTo(allAuthors)))
                .andExpect(model().attribute("all_publishers", equalTo(allPublishers)));
    }
}
