package com.github.reinis.bookregistryweb;

import com.github.reinis.bookregistryweb.dao.PublisherRepository;
import com.github.reinis.bookregistryweb.entity.Publisher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class PublisherRepositoryIntegrationTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PublisherRepository publisherRepository;

    @Test
    public void whenFindByName_thenReturnPublisher() {
        var given = new Publisher("Amyrlin Seat");
        entityManager.persist(given);
        entityManager.flush();

        Publisher found = publisherRepository
                .findByName(given.getName());

        assertThat(found.getName())
                .isEqualTo(given.getName());
    }
}
