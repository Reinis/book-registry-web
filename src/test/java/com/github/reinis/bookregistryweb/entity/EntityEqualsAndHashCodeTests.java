package com.github.reinis.bookregistryweb.entity;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/* Generic functions for testing entity behavior across state transitions.
 *
 * Receive a new entity as a parameter and test it.
 */
@Slf4j
@RequiredArgsConstructor
public class EntityEqualsAndHashCodeTests<T> {

    private final TestEntityManager entityManager;

    public void persistConsistency(T entity) {
        // Test data
        var tuple = Set.of(entity);

        // Test new to managed transition
        log.debug(">> New entity = {}", entity);
        entityManager.persist(entity);
        assertTrue(
                tuple.contains(entity),
                "The entity is not found in the tuple after it is persisted."
        );
        log.debug(">> Persist entity = {}", entity);

        // Test after the flush
        entityManager.flush();
        log.debug(">> Flush entity = {}", entity);
        assertTrue(
                tuple.contains(entity),
                "The entity is not found in the tuple after it is persisted."
        );
    }

    public void detachedConsistency(T entity) {
        // Test data
        var tuple = Set.of(entity);

        entityManager.persist(entity);
        entityManager.flush();

        // Test managed to detached transition
        entityManager.detach(entity);
        log.debug(">> Detach entity = {}", entity);
        assertFalse(
                entityManager.getEntityManager().contains(entity),
                "The entity is found in the current persistence context even after detach."
        );
        assertTrue(
                tuple.contains(entity),
                "The entity is not found in the tuple after it is detached."
        );
    }

    /* This fails if there is not a proper equals() or hashCode() method for the entity.
     */
    public void mergedConsistency(T entity, BiConsumer<T, String> fn) {
        // Test data
        var tuple = Set.of(entity);

        entityManager.persist(entity);
        entityManager.flush();
        entityManager.detach(entity);

        // Test detached to merged transition
        fn.accept(entity, "Amyrlin");
        T _entity = entityManager.merge(entity);
        log.debug(">> Merge _entity = {}", _entity);
        log.debug(">> Original entity = {}", entity);
        assertTrue(
                tuple.contains(_entity),
                "The entity is not found in the tuple after it is merged."
        );

        // Test after the flush
        entityManager.flush();
        log.debug(">> Flush entity = {}", entity);
        log.debug(">> Flush _entity = {}", _entity);
        assertTrue(
                tuple.contains(_entity),
                "The entity is not found in the tuple after it is merged."
        );
    }

    public void findConsistency(T entity, Class<T> cls, Function<T, Integer> fn) {
        // Test data
        var tuple = Set.of(entity);

        entityManager.persist(entity);
        entityManager.flush();

        // Test find
        T _entity = entityManager.find(cls, fn.apply(entity));
        log.debug(">> Find _entity = {}", _entity);
        log.debug(">> Original entity = {}", entity);
        assertTrue(
                tuple.contains(_entity),
                "The entity is not found in the tuple after it is found by the entity manager."
        );
    }

    public void removeConsistency(T entity) {
        // Test data
        var tuple = Set.of(entity);

        entityManager.persist(entity);
        entityManager.flush();

        // Test managed to removed transition
        entityManager.remove(entity);
        log.debug(">> Remove entity = {}", entity);
        assertTrue(
                tuple.contains(entity),
                "The entity is not found in the tuple after it is removed."
        );

        // Test after the flush
        entityManager.flush();
        log.debug(">> Flush entity = {}", entity);
        assertTrue(
                tuple.contains(entity),
                "The entity is not found in the tuple after it is removed."
        );
    }
}
