package com.github.reinis.bookregistryweb.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.annotation.PostConstruct;

@DataJpaTest
public class PublisherEntityConsistencyTests {

    @Autowired
    private TestEntityManager entityManager;

    private EntityEqualsAndHashCodeTests<Publisher> entityTester;

    private Publisher publisher;

    @PostConstruct
    public void setUpEntityTester() {
        entityTester = new EntityEqualsAndHashCodeTests<>(entityManager);
    }

    @BeforeEach
    public void setUpTestData() {
        // Test data
        publisher = new Publisher("Amyrlin Seat");
    }

    @Test
    public void persistConsistency() {
        // Test new to managed transition
        entityTester.persistConsistency(publisher);
    }

    @Test
    public void detachedConsistency() {
        // Test managed to detached transition
        entityTester.detachedConsistency(publisher);
    }

    /* This fails if there is not a proper equals() or hashCode() method for the entity.
     */
    @Test
    public void mergedConsistency() {
        // Test detached to merged transition
        entityTester.mergedConsistency(publisher, Publisher::setName);
    }

    @Test
    public void findConsistency() {
        // Test find
        entityTester.findConsistency(publisher, Publisher.class, Publisher::getId);
    }

    @Test
    public void removeConsistency() {
        // Test managed to removed transition
        entityTester.removeConsistency(publisher);
    }
}
