package com.github.reinis.bookregistryweb.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.annotation.PostConstruct;

@DataJpaTest
public class BookEntityConsistencyTests {

    @Autowired
    private TestEntityManager entityManager;

    private EntityEqualsAndHashCodeTests<Book> entityTester;

    private Book book;

    @PostConstruct
    public void setUpEntityTester() {
        entityTester = new EntityEqualsAndHashCodeTests<>(entityManager);
    }

    @BeforeEach
    public void setUpTestData() {
        // Test data
        var publisher = new Publisher("Tor Books");
        book = new Book("New Spring", 2004, publisher);
    }

    @Test
    public void persistConsistency() {
        // Test new to managed transition
        entityTester.persistConsistency(book);
    }

    @Test
    public void detachedConsistency() {
        // Test managed to detached transition
        entityTester.detachedConsistency(book);
    }

    /* This fails if there is not a proper equals() or hashCode() method for the entity.
     */
    @Test
    public void mergedConsistency() {
        // Test detached to merged transition
        entityTester.mergedConsistency(book, Book::setTitle);
    }

    @Test
    public void findConsistency() {
        // Test find
        entityTester.findConsistency(book, Book.class, Book::getId);
    }

    @Test
    public void removeConsistency() {
        // Test managed to removed transition
        entityTester.removeConsistency(book);
    }
}
