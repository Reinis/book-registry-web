package com.github.reinis.bookregistryweb.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.annotation.PostConstruct;

@DataJpaTest
public class AuthorEntityConsistencyTests {

    @Autowired
    private TestEntityManager entityManager;

    private EntityEqualsAndHashCodeTests<Author> entityTester;

    private Author author;

    @PostConstruct
    public void setUpEntityTester() {
        entityTester = new EntityEqualsAndHashCodeTests<>(entityManager);
    }

    @BeforeEach
    public void setUpTestData() {
        // Test data
        author = new Author("Moiraine", "Damodred");
    }

    @Test
    public void persistConsistency() {
        // Test new to managed transition
        entityTester.persistConsistency(author);
    }

    @Test
    public void detachedConsistency() {
        // Test managed to detached transition
        entityTester.detachedConsistency(author);
    }

    /* This fails if there is not a proper equals() or hashCode() method for the entity.
     */
    @Test
    public void mergedConsistency() {
        // Test detached to merged transition
        entityTester.mergedConsistency(author, Author::setLastName);
    }

    @Test
    public void findConsistency() {
        // Test find
        entityTester.findConsistency(author, Author.class, Author::getId);
    }

    @Test
    public void removeConsistency() {
        // Test managed to removed transition
        entityTester.removeConsistency(author);
    }
}
