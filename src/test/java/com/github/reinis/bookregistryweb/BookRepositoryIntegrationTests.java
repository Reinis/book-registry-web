package com.github.reinis.bookregistryweb;

import com.github.reinis.bookregistryweb.dao.BookRepository;
import com.github.reinis.bookregistryweb.entity.Author;
import com.github.reinis.bookregistryweb.entity.Book;
import com.github.reinis.bookregistryweb.entity.Publisher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class BookRepositoryIntegrationTests {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    BookRepository bookRepository;

    @Test
    public void whenFindByName_thenReturnBook() {
        Book given = new Book("Test book", 2020, new Publisher("My Books"));
        Author author = new Author("John", "Doe");
        given.setAuthors(List.of(author));
        entityManager.persist(given);
        entityManager.flush();

        Book found = bookRepository.findByTitle(given.getTitle());

        assertThat(found.getTitle())
                .isEqualTo(given.getTitle());
        assertThat(found.getAuthors().get(0))
                .isEqualTo(author);
        assertThat(found.getYearPublished())
                .isEqualTo(given.getYearPublished());
        assertThat(found.getPublisher())
                .isEqualTo(given.getPublisher());
    }
}
