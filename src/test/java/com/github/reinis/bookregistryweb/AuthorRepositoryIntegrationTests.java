package com.github.reinis.bookregistryweb;

import com.github.reinis.bookregistryweb.dao.AuthorRepository;
import com.github.reinis.bookregistryweb.entity.Author;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class AuthorRepositoryIntegrationTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void whenFindByName_thenReturnAuthor() {
        Author given = new Author("Alex", "Verus");
        entityManager.persist(given);
        entityManager.flush();

        Author found = authorRepository.findByName(given.getName());

        assertThat(found.getName())
                .isEqualTo(given.getName());
    }
}
