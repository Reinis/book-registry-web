-- Create tables
\i '/docker-entrypoint-initdb.d/db-scripts/book-registry-init.sql'

-- Add seed data for testing
\i '/docker-entrypoint-initdb.d/db-scripts/book-registry-populate.sql'
