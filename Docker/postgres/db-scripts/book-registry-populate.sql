BEGIN TRANSACTION;

-- publishers
INSERT INTO publishers (name)
VALUES
('Penguin Putnam Inc'),
('Tor Books');

-- authors
INSERT INTO authors (first_name, last_name)
VALUES
('Benedict', 'Jacka'),
('Brandon', 'Sanderson'),
('Robert', 'Jordan'),
('Ad', 'Lagendijk'),
('Ada', 'Lovelace');

-- books
INSERT INTO books (title, year_published, publisher_id)
VALUES
('Fated', 2012, 1),
('Mistborn: The Final Empire', 2006, 2),
('The Eye of the World', 1990, 2),
('A Memory of Light', 2013, 2);

-- book authors
INSERT INTO author_book_relations (author_id, book_id)
VALUES
(1, 1),
(2, 2),
(3, 3),
(3, 4),
(2, 4);

COMMIT;
