BEGIN TRANSACTION;

DROP TABLE IF EXISTS author_book_relations;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS publishers;

CREATE TABLE publishers (
	publisher_id int GENERATED ALWAYS AS IDENTITY,
	name varchar(255),
	PRIMARY KEY (publisher_id)
);

CREATE TABLE authors (
	author_id int GENERATED ALWAYS AS IDENTITY,
	first_name varchar(80),
	last_name varchar(80),
	PRIMARY KEY (author_id)
);

CREATE TABLE books (
	book_id int GENERATED ALWAYS AS IDENTITY,
	title varchar(255),
	year_published int,
	publisher_id int,
	CONSTRAINT fk_publisher
		FOREIGN KEY (publisher_id) REFERENCES publishers
		ON DELETE SET NULL,
	PRIMARY KEY (book_id)
);

CREATE TABLE author_book_relations (
	author_id int NOT NULL,
	book_id int NOT NULL,
	CONSTRAINT fk_author
		FOREIGN KEY (author_id) REFERENCES authors
		ON DELETE CASCADE,
	CONSTRAINT fk_book
		FOREIGN KEY (book_id) REFERENCES books
		ON DELETE CASCADE,
	PRIMARY KEY (author_id, book_id)
);

COMMIT;
