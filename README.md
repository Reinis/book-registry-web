
# Solution


1. Function to register a book with the following metadata — title, year
   published, publisher.

    - Add Book button gives a form to enter book metadata.
    - There is a multi-select input field to associate registered authors with
      the book.
        - An improvement would be to add author search to the selection form.

2. Function to register book authors. A book can have multiple authors (1 to n
   relation).

    - Add Author button gives a form to create a new author.

3. Function to search books by book metadata and authors.

    - Dropdown menu to select the book metadata to search for


## Technologies used


- Spring Boot 2.3.5
    - start.spring.io to generate a starter project
        - Project Metadata
            - **Group:** com.github.reinis
            - **Artifact:** book-registry-web
            - **Name:** book-registry-web
            - **Description:** Test project for book registry
            - **Package name:** com.github.reinis.bookregistryweb
            - **Packaging:** Jar
            - **Java:** 11
        - Dependencies
            - Spring Web Starter
            - Thymeleaf
            - Spring Data JPA
            - PostgreSQL Driver
            - Rest Repositories
                - Auto-generates REST API (see /auto-api)
            - Spring Boot DevTools
    - Spring Boot Data JPA with Hibernate for DB access
    - Thymeleaf for MVC View generation
        - jQuery and Bootstrap for webpage styling and interactive elements
        - Thymeleaf templates function as webpage prototypes since they are
          valid HTML files and can be opened in a browser as-is
- IntelliJ IDEA
    - Apache Tomcat 9
    - JDK 11
- VSCode for Thymleaf template and JS file editing
    - PlantUML for data model and DB structure description
- PostgreSQL 12 running in Docker container


```plantuml
@startuml
    title Components of the solution

    boundary WebApp #99ff99
    control Controller
    control Service
    control Repository
    database Database
    boundary AutoApi

    WebApp <-> Controller
    Controller <-> Service
    Service <-> Repository
    Repository <-> Database

@enduml
```

## Data model and Database structure

Authors of a book could be stored as a text array or in a separate redundant
table, but that would violate database normalization principle, so authors and
books really have many-to-many relation.

A similar issue is with the publishers, which I have moved to a separate table.
Books and the publishers have many-to-one relation — each book can have at most
one publisher. In general case, books can have multiple publishers, but I'm
leaving this a bit simpler here.

To model the many-to-many relation between the book and the author tables, the
third — join — table is necessary. The join table references only the
corresponding author and book ids.

These ids are stored as foreign keys with cascading deletes. This ensures that
the corresponding connections will be deleted when either an author or a book
is deleted. Deletes don't propagate beyond the join table.

```plantuml
@startuml
    title Database structure

    entity "Author" as authors {
        * author_id <<generated>>
        --
        first_name
        last_name
    }

    entity join {
        * author_id
        * book_id
    }

    entity "Book" as books {
        * book_id <<generated>>
        --
        * title
        year_published
        publisher_id
    }

    entity "Publisher" as publishers {
        * publisher_id <<generated>>
        --
        * name
    }

    authors ||-right-|{ join
    join }|-right-|| books
    books |o--o| publishers

@enduml
```

```plantuml
@startuml
    title Entities

    Class Author {
        - id
        - firstName
        - lastName
        ..
        - books
    }

    Class Book {
        - id
        - title
        - yearPublished
        ..
        - publisher
        - authors
    }

    Class Publisher {
        - id
        - name
    }

    Author }o--o{ Book
    Book |o--o| Publisher

@enduml
```

## Notes

The app needs a PostgreSQL server running on `localhost:5432` and it uses
`book-registry` database as user "test" with the password "secret" (it is
configured in `src/main/resources/application.properties`). I'm running it in
Docker. First, have to build the image that includes the SQL scripts for
creating the tables and inserting sample data:

```
# sudo docker build -t postgres:12 .
```

Here is a single command to run a throw-away container with all the
necessary tables created and seeded with a sample data:

```
# docker run --name pg-docker --rm -p 5432:5432 -e POSTGRES_PASSWORD=secret -e POSTGRES_USER=test -e POSTGRES_DB=book-registry -e POSTGRES_HOST=postgres -d postgres:12
```

It relies on the presence of the files in the postgres directory (run the
command from there). The relevant files are:

```
postgres/
├── db-scripts
│   ├── book-registry-init.sql
│   └── book-registry-populate.sql
├── deploy-schemas.sql
└── Dockerfile
```

The first of the two scripts in the db-scripts directory creates the tables and
the second adds the sample data.

> **Note:** There has been no attempt made to secure the database or the app in
> any way.  In production the app user should have no permissions to change
> database structure. And the app should have some sort of authentication and
> authorization for database actions. It could be implemented using Spring Boot
> Security framework, but I'm not quite that far in my Spring studies and I ran
> out of the time.

During development I built it in a single, self-contained JAR file with an
included tomcat server and ran it from IDEA, but to generate the WAR file, I
had to change my settings a bit (see `war` branch). I tested the WAR file on my
local Tomcat 9 installation on Arch Linux.


